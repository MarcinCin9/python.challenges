#convert faranheit to calcius method 1
convert_far_to_cel = input("Enter a temperature in degrees F: ")
far = float(convert_far_to_cel)
number1 = 32
number2 = 9
number3 = 5

result1 = far - number1
result2 = number3 / number2

multiply = result1 * result2

celcius = multiply
print( f"{convert_far_to_cel} degrees F = {celcius:.2f} degrees C")


#convert calcius to faranheit method 1
convert_cel_to_far = input("Enter a temperature in degrees C: ")

cel = float(convert_cel_to_far)
number1 = 32
number2 = 9
number3 = 5

result1 = cel * number2
result2 = result1 / number3

sum_num = result2 + number1

fahrenheit = sum_num
print( f"{convert_cel_to_far} degrees C = {fahrenheit:.2f} degrees F")


#convert faranheit to calcius method 2
convert_far_to_cel = float(input("Enter a temperature in degrees F: "))
far = convert_far_to_cel
number1 = 32
number2 = 9
number3 = 5

print( f"{far} degrees F = {(far - number1) * number3 / number2:.2f} degrees C")


#convert calcius to faranheit method 2
convert_cel_to_far = float(input("Enter a temperature in degrees C: "))
cel = convert_cel_to_far
number1 = 32
number2 = 9
number3 = 5

print(f"{cel} degrees C = {cel * number2 / number3 + number1:.2f} degrees F")
